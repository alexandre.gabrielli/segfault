# openapi-java-client

Users API
- API version: 0.1.0
  - Build date: 2020-07-05T09:52:27.383+02:00[Europe/Berlin]

An API to hand users with authentication/authorization


*Automatically generated by the [OpenAPI Generator](https://openapi-generator.tech)*


## Requirements

Building the API client library requires:
1. Java 1.7+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>org.openapitools</groupId>
  <artifactId>openapi-java-client</artifactId>
  <version>0.1.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "org.openapitools:openapi-java-client:0.1.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/openapi-java-client-0.1.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String userId = "userId_example"; // String | 
    String authorization = "authorization_example"; // String | 
    String password = "password_example"; // String | 
    try {
      apiInstance.changePassword(userId, authorization, password);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#changePassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080/api-auth*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**changePassword**](docs/DefaultApi.md#changePassword) | **PATCH** /users/{userId} | 
*DefaultApi* | [**createUser**](docs/DefaultApi.md#createUser) | **POST** /users | 
*DefaultApi* | [**deleteUser**](docs/DefaultApi.md#deleteUser) | **DELETE** /users/{userId} | 
*DefaultApi* | [**getUsers**](docs/DefaultApi.md#getUsers) | **GET** /users | 
*DefaultApi* | [**login**](docs/DefaultApi.md#login) | **POST** /connection | 


## Documentation for Models

 - [Credentials](docs/Credentials.md)
 - [Token](docs/Token.md)
 - [User](docs/User.md)
 - [UserDTO](docs/UserDTO.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



