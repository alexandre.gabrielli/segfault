# DefaultApi

All URIs are relative to *http://localhost:8080/api-auth*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePassword**](DefaultApi.md#changePassword) | **PATCH** /users/{userId} | 
[**createUser**](DefaultApi.md#createUser) | **POST** /users | 
[**deleteUser**](DefaultApi.md#deleteUser) | **DELETE** /users/{userId} | 
[**getUsers**](DefaultApi.md#getUsers) | **GET** /users | 
[**login**](DefaultApi.md#login) | **POST** /connection | 


<a name="changePassword"></a>
# **changePassword**
> changePassword(userId, authorization, password)



modify the password of a user

### Example
```java
// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String userId = "userId_example"; // String | 
    String authorization = "authorization_example"; // String | 
    String password = "password_example"; // String | 
    try {
      apiInstance.changePassword(userId, authorization, password);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#changePassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**|  |
 **authorization** | **String**|  |
 **password** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | password modified successfully |  -  |
**403** | forbidden |  -  |
**404** | user not found |  -  |

<a name="createUser"></a>
# **createUser**
> createUser(authorization, user)



create a user

### Example
```java
// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    User user = new User(); // User | 
    try {
      apiInstance.createUser(authorization, user);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#createUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |
 **user** | [**User**](User.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | user created successfully |  -  |
**403** | forbidden |  -  |
**409** | email already used |  -  |

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(userId, authorization)



delete a user

### Example
```java
// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String userId = "userId_example"; // String | 
    String authorization = "authorization_example"; // String | 
    try {
      apiInstance.deleteUser(userId, authorization);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#deleteUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**|  |
 **authorization** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | user deleted successfully |  -  |
**403** | forbidden |  -  |
**404** | user not found |  -  |

<a name="getUsers"></a>
# **getUsers**
> List&lt;UserDTO&gt; getUsers(authorization)



get the list of all users

### Example
```java
// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    String authorization = "authorization_example"; // String | 
    try {
      List<UserDTO> result = apiInstance.getUsers(authorization);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#getUsers");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  |

### Return type

[**List&lt;UserDTO&gt;**](UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | users retrieved successfully |  -  |
**403** | forbidden |  -  |

<a name="login"></a>
# **login**
> Token login(credentials)



user connection

### Example
```java
// Import classes:
import io.heig.users.ApiClient;
import io.heig.users.ApiException;
import io.heig.users.Configuration;
import io.heig.users.models.*;
import io.heig.users.api.DefaultApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080/api-auth");

    DefaultApi apiInstance = new DefaultApi(defaultClient);
    Credentials credentials = new Credentials(); // Credentials | 
    try {
      Token result = apiInstance.login(credentials);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DefaultApi#login");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**Credentials**](Credentials.md)|  |

### Return type

[**Token**](Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successfuly loged in |  -  |
**401** | unvalid credentials |  -  |
**500** | token generation failure |  -  |

