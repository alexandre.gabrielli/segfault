$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("authentication.feature");
formatter.feature({
  "line": 1,
  "name": "Authentication of Users",
  "description": "",
  "id": "authentication-of-users",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 997674120,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful authentication of existing admin user",
  "description": "",
  "id": "authentication-of-users;successful-authentication-of-existing-admin-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have an admin credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a 200 status code",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload()"
});
formatter.result({
  "duration": 124354,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 15464659,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(authentication.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 922986,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "failed authentication with wrong password",
  "description": "",
  "id": "authentication-of-users;failed-authentication-with-wrong-password",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I have an admin credentials payload with wrong password",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I receive a 401 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload_with_wrong_password()"
});
formatter.result({
  "duration": 53242,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 215886,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(authentication.feature:14)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "401",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 989721,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "failed authentication with none existing user",
  "description": "",
  "id": "authentication-of-users;failed-authentication-with-none-existing-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "I have a user credentials payload with wrong id",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I receive a 401 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_a_user_credentials_payload_with_wrong_id()"
});
formatter.result({
  "duration": 45219,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 264388,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(authentication.feature:19)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "401",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("creation.feature");
formatter.feature({
  "line": 1,
  "name": "Creation of Users",
  "description": "",
  "id": "creation-of-users",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 761436,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful creation of a user",
  "description": "",
  "id": "creation-of-users;successful-creation-of-a-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have an admin credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I have a user payload",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I POST it to the /users endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I receive a 201 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload()"
});
formatter.result({
  "duration": 37562,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 416091,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(creation.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_have_a_user_payload()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_POST_it_to_the_users_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 1399247,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "creation attempt of an existing user",
  "description": "",
  "id": "creation-of-users;creation-attempt-of-an-existing-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "I have an admin credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I have an existing user payload",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "I POST it to the /users endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I receive a 409 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload()"
});
formatter.result({
  "duration": 67464,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 762894,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(creation.feature:16)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_have_an_existing_user_payload()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_POST_it_to_the_users_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "409",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 946325,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "creation attempt by an unauthorized user",
  "description": "",
  "id": "creation-of-users;creation-attempt-by-an-unauthorized-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "I have a standard user credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I have a user payload",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "I POST it to the /users endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_a_standard_user_credentials_payload()"
});
formatter.result({
  "duration": 43031,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 268399,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(creation.feature:24)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_have_a_user_payload()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreationSteps.i_POST_it_to_the_users_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("deletion.feature");
formatter.feature({
  "line": 1,
  "name": "Deletion of a User",
  "description": "",
  "id": "deletion-of-a-user",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 790610,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful deletion of a user",
  "description": "",
  "id": "deletion-of-a-user;successful-deletion-of-a-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have an admin credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I POST it to the /users/bob@mail.com endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I receive a 204 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload()"
});
formatter.result({
  "duration": 30268,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 343156,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(deletion.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DeletionSteps.i_POST_it_to_the_users_bob_mail_com_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "204",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 769823,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "deletion attempt of none existing user",
  "description": "",
  "id": "deletion-of-a-user;deletion-attempt-of-none-existing-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "I have an admin credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "I POST it to the /users/fake@mail.com endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I receive a 404 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_an_admin_credentials_payload()"
});
formatter.result({
  "duration": 24797,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 287726,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(deletion.feature:15)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DeletionSteps.i_POST_it_to_the_users_fake_mail_com_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "404",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 783316,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "unauthorized attempt of user deletion",
  "description": "",
  "id": "deletion-of-a-user;unauthorized-attempt-of-user-deletion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "I have a standard user credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "I POST it to the /users/bob@mail.com endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_a_standard_user_credentials_payload()"
});
formatter.result({
  "duration": 23339,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 251259,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(deletion.feature:22)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DeletionSteps.i_POST_it_to_the_users_bob_mail_com_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("retrieval.feature");
formatter.feature({
  "line": 1,
  "name": "Retrieval of Users",
  "description": "",
  "id": "retrieval-of-users",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 863179,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "successful retrieval of the users",
  "description": "",
  "id": "retrieval-of-users;successful-retrieval-of-the-users",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "I have a standard user credentials payload",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I POST it to the /connection endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I receive a token",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I GET the /users endpoint",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I receive a 200 status code",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I receive an array of usersDTO",
  "keyword": "Then "
});
formatter.match({
  "location": "AuthenticationSteps.i_have_a_standard_user_credentials_payload()"
});
formatter.result({
  "duration": 26986,
  "status": "passed"
});
formatter.match({
  "location": "AuthenticationSteps.i_POST_it_to_the_connection_endpoint()"
});
formatter.result({
  "duration": 357744,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.loginCall(DefaultApi.java:610)\r\n\tat io.heig.users.api.DefaultApi.loginValidateBeforeCall(DefaultApi.java:622)\r\n\tat io.heig.users.api.DefaultApi.loginWithHttpInfo(DefaultApi.java:661)\r\n\tat io.heig.users.api.spec.steps.AuthenticationSteps.i_POST_it_to_the_connection_endpoint(AuthenticationSteps.java:43)\r\n\tat ✽.When I POST it to the /connection endpoint(retrieval.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "AuthenticationSteps.i_receive_a_token()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "RetrievalSteps.i_GET_the_users_endpoint()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "RetrievalSteps.i_receive_an_array_of_usersDTO()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "there is a Users server",
  "keyword": "Given "
});
formatter.match({
  "location": "AuthenticationSteps.there_is_a_Users_server()"
});
formatter.result({
  "duration": 793892,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "unauthorized retrieval of users attempt",
  "description": "",
  "id": "retrieval-of-users;unauthorized-retrieval-of-users-attempt",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "I GET the /users endpoint with invalid token",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I receive a 403 status code",
  "keyword": "Then "
});
formatter.match({
  "location": "RetrievalSteps.i_GET_the_users_endpoint_with_invalid_token()"
});
formatter.result({
  "duration": 377801,
  "error_message": "java.lang.IllegalArgumentException: Expected URL scheme \u0027http\u0027 or \u0027https\u0027 but no colon was found\r\n\tat okhttp3.HttpUrl$Builder.parse$okhttp(HttpUrl.kt:1257)\r\n\tat okhttp3.HttpUrl$Companion.get(HttpUrl.kt:1630)\r\n\tat okhttp3.Request$Builder.url(Request.kt:184)\r\n\tat io.heig.users.ApiClient.buildRequest(ApiClient.java:1024)\r\n\tat io.heig.users.ApiClient.buildCall(ApiClient.java:999)\r\n\tat io.heig.users.api.DefaultApi.getUsersCall(DefaultApi.java:495)\r\n\tat io.heig.users.api.DefaultApi.getUsersValidateBeforeCall(DefaultApi.java:507)\r\n\tat io.heig.users.api.DefaultApi.getUsersWithHttpInfo(DefaultApi.java:544)\r\n\tat io.heig.users.api.spec.steps.RetrievalSteps.i_GET_the_users_endpoint_with_invalid_token(RetrievalSteps.java:49)\r\n\tat ✽.When I GET the /users endpoint with invalid token(retrieval.feature:15)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 12
    }
  ],
  "location": "AuthenticationSteps.i_receive_a_status_code(int)"
});
formatter.result({
  "status": "skipped"
});
});