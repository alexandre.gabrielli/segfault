package io.heig.users.business;

import com.auth0.jwt.exceptions.JWTCreationException;
import io.heig.users.api.model.Token;
import io.heig.users.api.model.User;
import io.heig.users.business.models.AuthInfo;

public interface IAuthorizationService {
    Token generateToken(User user) throws JWTCreationException;
    AuthInfo decodeToken(String token);
}
