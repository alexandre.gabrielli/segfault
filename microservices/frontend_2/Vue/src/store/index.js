import Vue from "vue";
import Vuex from "vuex";
import { auth } from "./auth.module";
import { pipeline } from "./pipeline.module";
import { yamlValidator } from "./yamlValidator.module";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    pipeline,
    yamlValidator
  }
});
