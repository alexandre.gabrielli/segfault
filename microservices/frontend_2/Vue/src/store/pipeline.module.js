import PipelineService from "../services/pipeline.service";

export const pipeline = {
  namespaced: true,
  state: {},
  actions: {
    /**
     * envois et sauvegarde une description de pipeline pour l'utilisateur courrant
     * @param {json} JSONdescription une description json d'un pipeline
     */
    // eslint-disable-next-line no-unused-vars
    createDescription({ commit }, JSONdescription) {
      return PipelineService.createDescription(JSONdescription).then(
        (creationDate) => {
          return Promise.resolve(creationDate);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    // eslint-disable-next-line no-unused-vars
    getDescriptions({ commit }) {
      return PipelineService.getDescriptions().then(
        (descriptionMap) => {
          return Promise.resolve(descriptionMap);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },

    // eslint-disable-next-line no-unused-vars
    getDescription({ commit }, description) {
      return PipelineService.getDescription(description).then(
        (description) => {
          return Promise.resolve(description);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },

    // eslint-disable-next-line no-unused-vars
    create({ commit }, description) {
      return PipelineService.create(description).then(
        (pipeline) => {
          return Promise.resolve(pipeline);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    // eslint-disable-next-line no-unused-vars
    getPipelines({ commit }) {
      return PipelineService.getPipelines().then(
        (pipelines) => {
          return Promise.resolve(pipelines);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    // eslint-disable-next-line no-unused-vars
    getPipeline({ commit }, pipeline) {
      return PipelineService.getPipeline(pipeline).then(
        (pipeline) => {
          return Promise.resolve(pipeline);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    // eslint-disable-next-line no-unused-vars
    getProgress({ commit }, pipeline) {
      return PipelineService.getProgress(pipeline).then(
        (progress) => {
          return Promise.resolve(progress);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    // eslint-disable-next-line no-unused-vars
    getResults({ commit }, pipeline) {
      return PipelineService.getResults(pipeline).then(
        (results) => {
          return Promise.resolve(results);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {}
};
