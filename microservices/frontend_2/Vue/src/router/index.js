import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/descriptionCreator",
    name: "descriptionCreator",
    component: () => import("../views/descriptionCreator.vue")
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/Register.vue")
  },
  {
    path: "/newpipeline",
    name: "NewPipeline",
    component: () => import("../views/newPipeline.vue")
  },
  {
    path: "/pipelineSelector",
    name: "PipelineSelector",
    component: () => import("../views/PipelineSelector")
  },
  {
    path: "/oldPipelineSelector",
    name: "oldPipelineSelector",
    component: () => import("../views/oldPipelineSelector")
  },
  {
    path: "/pipelineParameter",
    name: "PipelineParameter",
    props: true,
    component: () => import("../views/PipelineParameter")
  },
  {
    path: "/pipelineRenderer",
    name: "PipelineRenderer",
    props: true,
    component: () => import("../views/PipelineRenderer")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
