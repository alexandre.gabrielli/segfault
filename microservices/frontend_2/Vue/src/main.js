import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VeeValidate from "vee-validate";

//import bulma
require("./assets/main.scss");

//font-awesome
import { library } from "@fortawesome/fontawesome-svg-core";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import {
  faEnvelope,
  faLock,
  faUser,
  faCodeBranch,
  faSearch,
  faHome
} from "@fortawesome/free-solid-svg-icons";

library.add(faEnvelope, faLock, faUser, faCodeBranch, faSearch, faHome);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.use(VeeValidate);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
