const env = process.env;

const config = {
  // If you don't want mongodb+srv, setup it in a env var
  API_URL: env.API_URL || "http://localhost:3000/orchestrator/pipeline/",
  AUTH_API: env.AUTH_API || "http://localhost:8080/api-auth/"
};

module.exports = { config };
