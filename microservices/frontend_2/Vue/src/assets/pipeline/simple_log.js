module.exports = {
  parameters: {
    repositories:
      "$$l{veuillez entrez la liste des repo que vous voulez analyser pour ce pipeline}$$"
  },
  clone: {
    input: false,
    params: "${repositories}",
    output: "repoId_${repositories}"
  },
  log: {
    input: "repoId_${repositories}",
    concatenation: true,
    output: "result.csv"
  }
};
