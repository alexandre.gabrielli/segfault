var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");

// declaration of variable variable
const { UsersPipelines } = require("./models/usersPipelines");
const { UsersDescriptions } = require("./models/usersDescriptions");
const { PipelineServices } = require("./service/pipelines.service");
const pipelineServices = new PipelineServices();
global.usersDescriptions = new UsersDescriptions(pipelineServices);
global.usersPipelines = new UsersPipelines(pipelineServices);

pipelineServices.initialise(usersDescriptions, usersDescriptions.artifact);
pipelineServices.initialise(usersPipelines, usersPipelines.artifact);

// todo : don't need index ?
var indexRouter = require("./routes/index");
var router = require("./routes/users");

var app = express();
var ws = require("./ws");
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

var corsOptions = {
  //http://192.168.99.100
  origin: "http://localhost:8080",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

// todo : don't need index ?
app.use("/", indexRouter);
app.use("/orchestrator", router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
