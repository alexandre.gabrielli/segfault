const { Task, CloneTask, LogTask, AnalyseTask } = require("./task");
const task = require("./task");

class UsersPipelines extends Map {
  constructor(pipelineServices) {
    super();
    this.pipelineServices = pipelineServices;
    this.artifact = "pipeline.json";
  }
  async addPipeline(user, description) {
    let date = new Date().valueOf();
    let path = [user, description.name, description.version, date];
    let pipeline = await this.prepare(description, date);
    await this.pipelineServices.addToMap(pipeline, this, path);
    await this.pipelineServices.createFolder(path, "repositoriesVolume");
    await this.pipelineServices.createFolder(path, "resultsVolume");
    await this.pipelineServices.createFolder(path, "pipelinesVolume");
    return pipeline;
  }

  async run(pipeline, user) {
    pipeline = this[user][pipeline.name][pipeline.version][pipeline.date];
    let path = `${user}/${pipeline.name}/${pipeline.version}/${pipeline.date}`;
    let dependency = new Map();
    let errors;
    if (pipeline.tasks.clone != undefined) {
      for (let task of pipeline.tasks.clone) {
        let cloneTask = new CloneTask(task.parameter, `${path}`, task.artifact);
        try {
          var res = await cloneTask.run(dependency);
        } catch (err) {
          console.log(err);
          res = false;
        }
        if (!res) {
          errors += `error when clone ${task.parameter}`;
        }
      }
      this.substrateOneRemaningTask(pipeline);
    }
    if (pipeline.tasks.logs != undefined) {
      for (let task of pipeline.tasks.logs) {
        let logsTask = new LogTask(path, task.artifact, task.dependency);
        let res = await logsTask.run(dependency);
        if (!res) {
          errors += `error when log ${task.artifact}`;
        }
      }
      this.substrateOneRemaningTask(pipeline);
    }

    if (pipeline.tasks.analyse != undefined) {
      for (let task of pipeline.tasks.analyse) {
        let anaylseTask = new AnalyseTask(path, task.analyse);
        let res = await anaylseTask.run();
        if (!res) {
          errors += `error when analyse `;
        }
      }
      this.substrateOneRemaningTask(pipeline);
    }
    this.putRemaningTaskToZero(pipeline);

    let clearTask = new Task("readwrite", `cd ${path} && rm -R *`);
    await clearTask.run();

    await this.pipelineServices.save(
      pipeline,
      path,
      "pipelinesVolume",
      "pipeline.json"
    );

    if (errors != null) {
      throw new Error(errors);
    }
  }

  putRemaningTaskToZero(pipeline) {
    pipeline.numberOfTasksRemaining = 0;
    if (pipeline.progressListener != undefined) {
      pipeline.progressListener(0);
    }
  }

  async prepare(description, date) {
    delete description.creationDate;
    description.date = date;
    if (description.tasks != undefined && description.tasks.length > 0) {
      let tasks = new Map();
      for (let task of description.tasks) {
        let variablesToReplace = new Set();
        let type = Object.getOwnPropertyNames(task)[0];
        if (type != undefined && !tasks.hasOwnProperty(type)) {
          tasks[type] = [];
        }
        for (const property in task[type]) {
          if (typeof task[type][property] != "string") {
            continue;
          }
          let variables = task[type][property].match(/{{.+?}}/g);
          if (variables != null) {
            for (let variable of variables) {
              variablesToReplace.add(variable);
            }
          }
        }

        if (variablesToReplace.size != 0) {
          for (let variableToReplace of variablesToReplace) {
            let variable;
            for (let potentialVariable of description.variables) {
              if (
                potentialVariable.name ==
                variableToReplace.replace(/{|}| /g, "")
              ) {
                variable = potentialVariable;
              }
            }
            if (variable.type == "list") {
              for (let newValue of variable.value) {
                let newTask = JSON.parse(JSON.stringify(task));
                for (const property in newTask[type]) {
                  if (typeof task[type][property] != "string") {
                    continue;
                  }
                  newTask[type][property] = newTask[type][property].replace(
                    variableToReplace,
                    newValue
                  );
                  if (property == "dependency" || property == "artifact") {
                    newTask[type][property] = newTask[type][property].replace(
                      /\/|:|\.| /g,
                      ""
                    );
                  }
                }
                tasks[type].push(newTask[type]);
              }
            }
          }
        } else {
          tasks[type].push(task);
        }
      }
      description.numberOfTasks = description.tasks.length;
      description.numberOfTasksRemaining = description.tasks.length;

      description.tasks = tasks;
    } else {
      throw new Error("no tasks");
    }
    delete description.variables;
    return description;
  }

  substrateOneRemaningTask(pipeline) {
    pipeline.numberOfTasksRemaining -= 1;

    if (pipeline.progressListener != undefined) {
      pipeline.progressListener(pipeline.numberOfTasksRemaining, pipeline.user);
    }
  }
  registerProgressListener(pipeline, listener) {
    pipeline.progressListener = listener;
    listener(pipeline.numberOfTasksRemaining);
  }

  getPipeline(user, name, version, date) {
    return this[user][name][version][date];
  }

  async getResults(user, name, version, date) {
    let artifacts = {};
    let path = `${user}/${name}/${version}/${date}`;
    let getTask = new Task(
      "readwrite",
      `ls resultsVolume/${path} | tr '\n' ','`
    );
    let result = await getTask.run();
    console.log(result);
    result = result.split(/\,/g).filter(function (el) {
      return el.length != 0;
    });
    console.log("result");
    console.log(result);
    for (let artifact of result) {
      console.log(artifact);
      let catTask = new Task(
        "readwrite",
        `cat resultsVolume/${path}/${artifact}`
      );
      let result = await catTask.run();
      console.log("artifact result");
      console.log(result);
      artifacts[artifact] = result;
    }
    return artifacts;
  }
  async getAll(user) {
    return this[user];
  }
}

module.exports = { UsersPipelines };
