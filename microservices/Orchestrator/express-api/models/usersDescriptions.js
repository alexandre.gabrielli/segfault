class UsersDescriptions extends Map {
  constructor(pipelineServices) {
    super();
    this.pipelineServices = pipelineServices;
    this.artifact = "description.json";
    this.savingVolume = "descriptionsVolume";
  }

  /**
   *  add a description to the user folder, if it's the first
   * time a folder would be creeted for this user
   * @param {JSON} jsondescription
   * @param {string} user
   */
  async addDescription(jsondescription, user, isprimary = false) {
    jsondescription.name = jsondescription.name.replace(/ /g, "_");
    jsondescription.version = jsondescription.version.replace(/ |\./g, "_");
    jsondescription.creationDate = new Date();
    this.verifie(jsondescription);
    if (!isprimary && !this.hasOwnProperty(user)) {
      this.addPrimaryPipeline(user);
    }
    let path = [user, jsondescription.name, jsondescription.version];
    await this.pipelineServices.addToMap(jsondescription, this, path);
    jsondescription.user = user;
    await this.pipelineServices.save(
      jsondescription,
      path,
      this.savingVolume,
      this.artifact
    );
    return true;
  }

  /***
   * throw an ReferenceError if a propriety missing or if she's invalid
   */
  verifie(jsondescription) {
    let message = "";
    const FolderRegex = /^[a-z|A-Z|0-9|_|.| ]+$/i;
    if (!jsondescription.hasOwnProperty("name")) {
      message += "no name ";
    } else if (!FolderRegex.test(jsondescription.name)) {
      message += "invalide format for name";
    }
    if (!jsondescription.hasOwnProperty("version")) {
      message += " no version";
    } else if (!FolderRegex.test(jsondescription.version)) {
      message += "invalide format for version";
    }
    if (!jsondescription.hasOwnProperty("tasks")) {
      message += " no tasks ";
    }
    if (message != "") {
      throw new ReferenceError(message);
    }
  }

  async addPrimaryPipeline(user) {
    return await this.addDescription(
      {
        name: "simple_pipeline",
        version: "1.0.0",
        variables: {
          repositoriesUrl: {
            type: "list",
            mark: "enter the list of repositrories?"
          }
        },
        tasks: [
          {
            clone: {
              parameter: "{{ repositoriesUrl }}",
              artifact: "{{ repositoriesUrl }}cloneartifact"
            }
          },
          {
            logs: {
              dependency: "{{ repositoriesUrl }}cloneartifact",
              artifact: "{{ repositoriesUrl }}logsartifact"
            }
          },
          {
            analyse: {
              artifact: "analyse.json",
              path: true,
              spring: true
            }
          }
        ]
      },
      user,
      true
    );
  }
  async getAll(user) {
    // if it's the first time add primary pipeline to user
    if (!this.hasOwnProperty(user)) {
      await this.addPrimaryPipeline(user);
    }
    return this[user];
  }

  get(name, version, user) {
    return this[user][name][version];
  }
}

module.exports = { UsersDescriptions };
