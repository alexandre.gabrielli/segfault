const { DockerService } = require("../service/docker.service");
let dockerService = new DockerService();

class Task {
  constructor(image, command) {
    this.image = image;
    this.command = command;
  }

  getCommand() {
    return this.command;
  }
  async run() {
    await dockerService.createAndRunImage(this);
    return this.result;
  }
}

class CloneTask extends Task {
  constructor(url, path, artifact) {
    super(
      "git",
      `cd repositoriesVolume/${path} && git clone ${url} >> clone.txt 2>&1`
    );
    this.path = path;
    this.artifact = artifact;
  }
  async run(dependency) {
    try {
      await dockerService.createAndRunImage(this);
      let repoTask = new Task(
        "readwrite",
        `cd repositoriesVolume/${this.path} && cat clone.txt `
      );
      let repoTaskResult = await repoTask.run();
      let result = this.parseResult(repoTaskResult);
      let cleanTask = new Task(
        "readwrite",
        `cd repositoriesVolume/${this.path} && rm clone.txt `
      );
      await cleanTask.run();
      dependency.set(this.artifact, { clone: result });
      console.log(dependency.get(this.artifact));
      let saveTaks = new Task(
        "readwrite",
        `cd resultsVolume/${this.path} && echo "${JSON.stringify({
          clone: result
        }).replace(/"/g, `\\"`)} " >> ${this.artifact} `
      );
      await saveTaks.run();
      return Promise.resolve(true);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  parseResult(result) {
    console.log(result);
    const regex = /Cloning into '.+?'.../g;
    var found = result.match(regex);
    if (found != null) {
      return found[0].replace(/Cloning into '|'.../g, "");
    } else {
      const regex = /fatal: destination path '.+?' already exists and is not an empty/g;
      const found = result.match(regex);
      if (found != null) {
        for (let repo of found) {
          result.push(
            repo.replace(
              /fatal: destination path '.|' already exists and is not an empty/g,
              ""
            )
          );
        }
        return result;
      } else {
        this.artifacts = "logERROR.txt";
        result = "clone ERROR : \n" + result + "\n\n";
        throw new Error(result);
      }
    }
  }
}

class LogTask extends Task {
  constructor(path, artifact, dependency) {
    super("git", null);
    this.path = path;
    this.artifact = artifact;
    this.dependency = dependency;
    this.prettyFormat = `--pretty=\'{"author":"%aN", "date" :"%ai" }\'`;
    this.command = `cd repositoriesVolume/${this.path} && git --no-pager log --use-mailmap ${this.prettyFormat}`;
    this.command = this.command.replace(/\r|\n/g, "");
  }
  async run(dependency) {
    let nomDuRepo = "t";
    if (this.dependency != undefined) {
      let getTaskResult = dependency.get(this.dependency);
      nomDuRepo = getTaskResult.clone;
      this.command = `cd repositoriesVolume/${this.path}/${nomDuRepo} && git --no-pager log --use-mailmap ${this.prettyFormat}`;
      this.command = this.command.replace(/\r|\n/g, "");
    }
    await dockerService.createAndRunImage(this);
    let result = this.parsing(this.result);
    if (this.artifact != undefined) {
      let savingObject = {};
      savingObject[`${nomDuRepo}`] = result;
      let saveResult = { log: savingObject };
      console.log(saveResult);
      let saveTaks = new Task(
        "readwrite",
        `cd resultsVolume/${this.path} && echo " ${JSON.stringify(
          saveResult
        ).replace(/"/g, `\\"`)} " &>> ${this.artifact} `
      );
      await saveTaks.run();
      return true;
    } else {
      return result;
    }
  }
  parsing(result) {
    let commitResults = [];
    let commits = result.match(/{.+?}/g);
    for (let commit in commits) {
      commitResults.push(JSON.parse(commits[commit]));
    }
    return commitResults;
  }
}

class AnalyseTask extends Task {
  constructor(path, task) {
    super(null, null);
    this.path = path;
    this.artifact = task.artifact;
    this.package = [
      {
        file: "package.json",
        name: "js",
        focus: "express vue",
        command: `npm ls express vue react --json`,
        parsingResult: function (information) {
          const regex = /{.+}/s;
          return JSON.parse(information.match(regex)[0]);
        }
      },
      {
        file: "pom.xml",
        name: "java",
        focus: "spring",
        command: "cat pom.xml",
        parsingResult: function (information) {
          const regex = new RegExp(`${this.focus}`, "g");
          let name = information.match(/<name>.+?<\/name>/g);
          if (name != null) {
            name = name[0].replace(/<name>|<\/name>/g, "");
          }
          let version = information.match(/<version>.+?<\/version>/g);
          if (version != null) {
            version = version[0].replace(/<version>|<\/version>/g, "");
          }
          let spring = information.search(regex) > -1 ? true : false;
          return { name: name, version: version, spring: spring };
        }
      }
    ];
  }

  async analyselanguage(language) {
    let analyseLanguageResult = [];

    let getPathToFileTask = new Task(
      "readwrite",
      `cd repositoriesVolume/${this.path} && find -name "${language.file}" -not -path "*/node_modules/*" `
    );
    let pathToFiles = await getPathToFileTask.run();
    if (pathToFiles != "") {
      pathToFiles = pathToFiles.split(/\r|\n/g).filter(function (el) {
        return el.length != 0;
      });
      for (let pathToFile of pathToFiles) {
        let informationJson = {};
        informationJson["path"] = pathToFile;
        let regex = new RegExp(`^\.|\/${language.file}`, "g");
        pathToFile = pathToFile.replace(regex, "");

        // recuperation des dependence
        let getModuleDependency = new Task(
          "node",
          `cd repositoriesVolume/${this.path}${pathToFile} && ${language.command}`
        );
        let dependencyInfo = await getModuleDependency.run();
        let jsonDependency = language.parsingResult(dependencyInfo);
        informationJson["dependency"] = jsonDependency;
        // recuperation des commits
        let logTask = new LogTask(`${this.path}/${pathToFile.split("/")[1]}`);
        logTask.command += `-p ${pathToFile.replace(/^.+?\//g, "")}`;
        let logs = await logTask.run();
        informationJson["commit"] = logs;
        analyseLanguageResult.push(informationJson);
      }
    }
    return analyseLanguageResult;
  }

  async save(analyseGlobalResult) {
    let savingTxt = JSON.stringify(analyseGlobalResult);
    let saveTask = new Task(
      "readwrite",
      `cd resultsVolume/${
        this.path
      } && echo "{ \\"analyse\\" : ${savingTxt.replace(/"/g, `\\"`)} }" > ${
        this.artifact
      } `
    );
    await saveTask.run();
    return true;
  }
  async run() {
    let analyseGlobalResult = {};
    for (let language of this.package) {
      analyseGlobalResult[language.name] = await this.analyselanguage(language);
    }
    return await this.save(analyseGlobalResult);
  }
}

module.exports = { Task, CloneTask, LogTask, AnalyseTask };
