const { Task } = require("../models/task");
const { AlreadyExistError } = require("../models/error");

class PipelineServices {
  constructor() {}

  async addToMap(objectToSave, map, path, count = 0) {
    if (count < path.length - 1) {
      if (!map.hasOwnProperty(path[count])) {
        map[path[count]] = new Map();
      }
      await this.addToMap(objectToSave, map[path[count]], path, ++count);
    } else {
      if (!map.hasOwnProperty(path[count])) {
        map[path[count]] = objectToSave;
      } else {
        throw new AlreadyExistError("already existe");
      }
    }
  }

  async save(objectToSave, path, volume, artifact, count = 0) {
    volume += `/${path[count]}`;
    if (count < path.length - 1) {
      let mkdirTask = new Task("readwrite", `mkdir ${volume}`);
      await mkdirTask.run();
      await this.save(objectToSave, path, volume, artifact, ++count);
    } else {
      let mkdirTask = new Task("readwrite", `mkdir ${volume}`);
      await mkdirTask.run();
      let saveTask = new Task(
        "readwrite",
        `cd ${volume} && echo "${JSON.stringify(objectToSave).replace(
          /"/g,
          `\\"`
        )}" > ${artifact}`
      );
      await saveTask.run();
    }
  }

  async createFolder(path, volume, count = 0) {
    volume += `/${path[count]}`;
    if (count < path.length) {
      let mkdirTask = new Task("readwrite", `mkdir ${volume}`);
      await mkdirTask.run();
      await this.createFolder(path, volume, ++count);
    }
  }

  async initialise(map, artifact) {
    let lsTask = new Task("readwrite", `find -name ${artifact}`);
    let paths = await lsTask.run();
    const regex = new RegExp(`.+\/.+\/${artifact}`, "g");
    let parsedpaths = paths.match(regex);
    if (parsedpaths != null) {
      for (let path of parsedpaths) {
        let getDescriptionTask = new Task("readwrite", `cat ${path}`);
        let jsonstringpipeline = await getDescriptionTask.run();
        let paths = path.split("/").slice(2, -1);
        let jsondescription = JSON.parse(
          jsonstringpipeline.replace(/\n|\r/, "")
        );
        jsondescription = jsondescription;
        this.addToMap(jsondescription, map, paths);
      }
    }
  }
}

module.exports = { PipelineServices };
